import React, { Component } from "react";
import "./FormApp.css";

export default class FormApp extends Component {
  state = {
    data: {
      name: "",
      phone: "",
      address: "",
      graduation: "",
      progress: "",
    },
    dataCopy: [],
    erroralert: false,
    display_message: false,
  };
  handleChange(e) {
    let { name } = e.target;
    let { value } = e.target;
    let { data } = this.state;
    data[name] = value;
    // console.log("data", e.target.value);
    this.setState({
      data,
    });
  }

  handleSubmit(e) {
    // console.log("ok");
    let { name, address, phone, graduation, progress } = this.state.data;
    if (
      name.length === 0 ||
      phone.length === 0 ||
      address.length === 0 ||
      graduation.length === 0 ||
      progress.length === 0
    ) {
      this.setState({
        erroralert: true,
      });
      return;
    }
    let aasd = {
      data: {
        name: this.state.data.name,
        phone: this.state.data.phone,
        address: this.state.data.address,
        graduation: this.state.data.graduation,
        progress: this.state.data.progress,
      },
    };
    this.setState((prevstate) => ({
      erroralert: false,
      display_message: true,
      dataCopy: prevstate.dataCopy.concat(aasd),
      data: {
        name: "",
        phone: "",
        address: "",
        graduation: "",
        progress: "",
      },
    }));
  }
  render() {
    let { name, address, phone, graduation, progress } = this.state.data;
    return (
      <div class='text-center'>
        <div class='p-3 mb-2 bg-info text-white'>
          <h1>Form of Students</h1>
        </div>
        <React.Fragment>
          <div className='container-fluid'>
            <div className='row'>
              <div className='col-md-3 mx-auto'>
                <div class='col-md-12'>
                  <label> Name</label>
                  <input
                    type='text'
                    class='form-control'
                    placeholder='First name'
                    value={name}
                    name='name'
                    onChange={(e) => this.handleChange(e)}
                  />
                  {this.state.erroralert && name.length === 0 ? (
                    <p style={{ color: "red" }}>Name is required</p>
                  ) : null}
                </div>

                <div class='col-md-12'>
                  <label> Phone.no</label>
                  <input
                    type='number'
                    class='form-control'
                    placeholder='Phone.no'
                    value={phone}
                    name='phone'
                    onChange={(e) => this.handleChange(e)}
                  />
                  {this.state.erroralert && name.length === 0 ? (
                    <p style={{ color: "red" }}>phone is required</p>
                  ) : null}
                </div>
                <div className='col-md-12'>
                  <label> Address</label>
                  <input
                    type='text'
                    class='form-control'
                    placeholder='Address'
                    value={address}
                    name='address'
                    onChange={(e) => this.handleChange(e)}
                  />
                  {this.state.erroralert && address.length === 0 ? (
                    <p style={{ color: "red" }}>Address is required</p>
                  ) : null}
                </div>
                <div className='col-md-12'>
                  <label> Graduation</label>
                  <input
                    type='text'
                    class='form-control'
                    placeholder='Graduation'
                    value={graduation}
                    name='graduation'
                    onChange={(e) => this.handleChange(e)}
                  />
                  {this.state.erroralert && graduation.length === 0 ? (
                    <p style={{ color: "red" }}>Graduation is required</p>
                  ) : null}
                </div>
                <div className='col-md-12'>
                  <label> Progress</label>
                  <input
                    type='text'
                    class='form-control'
                    placeholder='Progrress'
                    value={progress}
                    name='progress'
                    onChange={(e) => this.handleChange(e)}
                  />
                  {this.state.erroralert && progress.length === 0 ? (
                    <p style={{ color: "red" }}>Progress is required</p>
                  ) : null}
                </div>
              </div>
            </div>
          </div>
          <br />
          <button
            type='button'
            class='btn btn-outline-success'
            onClick={() => this.handleSubmit()}
          >
            Submit
          </button>
        </React.Fragment>
        <br />
        <br />
        <br />
        {this.state.display_message === true && (
          <div class='text-center'>
            <div class='p-3 mb-2 bg-secondary text-white'>
              <h1>Form of student</h1>
            </div>

            <table class='table table-striped'>
              <thead>
                <tr>
                  <th scope='col'>Name</th>
                  <th scope='col'>Address</th>
                  <th scope='col'>Phone</th>
                  <th scope='col'>Graduation</th>
                  <th scope='col'>progress</th>
                </tr>
              </thead>
              <tbody>
                {this.state.dataCopy.map((item) => (
                  <tr>
                    <td>{item.data.name}</td>
                    <td>{item.data.address}</td>
                    <td> {item.data.phone}</td>
                    <td>{item.data.graduation}</td>
                    <td>{item.data.progress}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        )}
      </div>
    );
  }
}

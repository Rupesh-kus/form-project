import React, { Component } from "react";
import FormApp from "./FormApp";

export default class App extends Component {
  render() {
    return (
      <div>
        <FormApp />
      </div>
    );
  }
}
